#!/usr/bin/env bash

# https://github.com/AladW/aurutils/issues/423

pkg="$1";
repo="$2";

pkg_dir=`find $HOME/.cache/aurutils/sync/ -maxdepth 1 -name "$pkg"`;

if [ -d "$pkg_dir" ]
then
    cd "$pkg_dir";
    aur build -fR -d "$repo";
    sudo pacman -S "$pkg";
else
   echo "Error: no already built package found!";
fi

#!/bin/bash

set -uo pipefail

repo_name=$1

remote_path=arch_repo-opal6:apps/arch_repo/$repo_name
local_path=$HOME/.local/packages/$repo_name

echo "## $local_path => $remote_path"

rclone sync "$local_path" "$remote_path" --progress --links


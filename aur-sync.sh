#!/bin/bash

set -uo pipefail

repo_name=$1

remote_path=arch_repo-opal6:apps/arch_repo/$repo_name
local_path=$HOME/.local/packages/$repo_name

if [ $repo_name != "machine" ]
   then
	   echo "## $remote_path => $local_path"
	   rclone sync "$remote_path" "$local_path" --progress --links
	   echo
fi

aur sync --repo "$repo_name" ${@:2} || true

echo

echo "## Pacman sync"
sudo pacman -Sy

echo

echo "## Pacman cache cleaning"
paccache -r

if [ $repo_name != "machine" ]
   then
	   echo
	   echo "## $local_path => $remote_path"
	   rclone sync "$local_path" "$remote_path" --progress --links
fi



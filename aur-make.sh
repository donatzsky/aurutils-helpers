#!/bin/bash

# Build and add package to repository

# Usage: aur-make.sh [repo [margs]]

source ./config.sh

#### Settings ####

repo=$custom_repo

##################

if [[ $1 = "" && $repo = "" ]]
then
	echo "No repository configured"
	exit 1
elif [[ $1 = "" && $repo != "" ]]
then
	echo "Using repo:" $repo
elif [[ $1 != "" ]]
then
	echo "Using repo:" $1
	repo=$1
fi

aur build -d $repo --margs --clean,${@:2}
